#!/usr/bin/env bash

export PATH="$PATH"

# Exportando as variáveis dos containers
CONT_SONAR_NAME="sonarqube"

# Array com os containers
CONTAINERS=( "$CONT_SONAR_NAME"  )

# Função para matar e remover os containers
function clearcontainers(){
  for c in "${CONTAINERS[@]}"; do
    ISRUNNING=$(docker ps | grep -o "$c")
    if [ -n "$ISRUNNING"  ]; then
      docker kill $c
      docker rm $c
    else
      echo "Container $c parado"
      docker rm $c
    fi
  done
}

# Função para realizar o download das imagens dos containers
function pullcontainers(){
  for c in "${CONTAINERS[@]}"; do
    docker pull $c
  done
}

# Função para executar cada container
function runcontainers(){
  for c in "${CONTAINERS[@]}"; do
    if [ $c == $CONT_SONAR_NAME  ]; then
      docker run -d --name $CONT_SONAR_NAME \
      -p 9000:9000 \
      -p 9092:9092 \
      -v /sonardata/data:/opt/sonarqube/data \
      -v /sonardata/extensions:/opt/sonarqube/extensions \
      -e SONARQUBE_JDBC_USERNAME=sonar \
      -e SONARQUBE_JDBC_PASSWORD=sonar2016 \
      -e SONARQUBE_JDBC_URL=jdbc:postgresql://sonar.coehsogod59o.us-east-1.rds.amazonaws.com:5432/sonar \
      $CONT_SONAR_NAME
    fi
  done
}

# Função para checar se o container está rodando ou não
function statuscontainers(){
  for c in "${CONTAINERS[@]}"; do
    STATUS=$(docker inspect --format="{{ .State.Running  }}" $c)
    NAMES=$(docker inspect --format="{{ .Name  }}" $c | cut -d'/' -f2)
    if [ $STATUS == "true"  ]; then
      echo "Container $NAMES rodando"
    else
      echo "Container $NAMES parado"
      STATUS=$(docker inspect --format="{{ .State.Error  }}" $c)
      echo "Erro:" $STATUS
    fi
  done
}

# Função para buscar e apresentar os IPs de cada container
function getinfofromcontainers(){
  for c in "${CONTAINERS[@]}"; do
    IP=$(docker inspect --format="{{ .NetworkSettings.IPAddress  }}" $c)
    if [ $c == $CONT_SONAR_NAME  ]; then
      echo "O $CONT_SONAR_NAME está com o IP: $IP"
    fi
  done
}

# Função para o HealthCheck
function healthcheck(){
  clearcontainers
  pullcontainers
  runcontainers
  statuscontainers
  getinfofromcontainers
}
