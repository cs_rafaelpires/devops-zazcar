set -x
export PATH=$PATH

EMULATOR_LIST="${WORKSPACE}/.emulator_list"
AVD_LIST="${WORKSPACE}/.avd_list"
echo "" > ${EMULATOR_LIST}
echo "" > ${AVD_LIST}

function emulator_list {
	cat ${EMULATOR_LIST}
}

function launch_avd {
	if [ -z "${1}" ]
	then
		echo "Usage: ${0} avd_name" 1>&2
		echo $"\tavds_name:\tA avd_name must exist and cannot run for two or more process in same time" 1>&2
		exit 1
	fi
	${ANDROID_HOME}/tools/emulator \
		-wipe-data -gpu on -noaudio -scale 1.0 \
		-no-boot-anim -accel on -avd "${1}" -qemu -m 1024 &
	echo ${!} >> ${AVD_LIST}
}

function create_avd_from_file {
	if [ -z "${1}" ] || [ ! -f "${1}" ]
	then
		echo "Usage: ${0} avds_file" 1>&2
		echo $"\tavds_file:\tA text file with one avd name per line" 1>&2
		exit 1
	fi
	IDX=0
	IFS=$'\n'
	for avd in `cat $1`
	do
		local AVD_NAME="${JOB_NAME}-${BUILD_NUMBER}-${IDX}"
		echo "" | ${ANDROID_HOME}/tools/android create avd -n $AVD_NAME -t $avd -f --abi default/x86 || true
		launch_avd ${AVD_NAME}
		IDX=$((IDX+1))
	done
	unset IFS
}

function wait_for_avd_boot {
	IDX=0
	sleep 120
	for porta in `${ANDROID_HOME}/platform-tools/adb devices | grep emulator | cut -d'-' -f2 | awk '{ print $1 }'`
	do
		if echo `cat ${AVD_LIST}` | grep -q `lsof -n -i4TCP:${porta} | awk '{ print $2 }' | tail -n +2`
		then
		  echo "emulator-${porta}" >> ${EMULATOR_LIST}
			${ANDROID_HOME}/platform-tools/adb -s "emulator-${porta}" wait-for-device
			${ANDROID_HOME}/platform-tools/adb -s "emulator-${porta}" shell 'while [ ""`getprop dev.bootcomplete` != "1" ] ; do sleep 1; done'
		fi
	done
}

function finish_avds {
	kill `cat ${AVD_LIST}` || true
	sleep 5
	rm -rf ${HOME}/.android/avd/${JOB_NAME}-${BUILD_NUMBER}-*.*
}

function reset_all_ios_simulators {
	xcrun simctl list devices |
		grep "Shutdown" |
		grep -iv "unavailable" |
		cut -d "(" -f2 |
		cut -d ")" -f1 |
		xargs -I {} sh -c 'xcrun simctl erase "{}";'
}

function unlock_keychain {
	security -v unlock-keychain -p "Concrete2015" "/Users/concrete/Library/Keychains/login.keychain"
}

set +x
