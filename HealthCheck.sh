#!/usr/bin/env bash

export PATH="$PATH"
source /root/BaseFunctionsContainers.sh

# Exportando as variáveis dos containers
CONT_SONAR_NAME="sonarqube"

# Array com os containers
CONTAINERS=( "$CONT_SONAR_NAME" )

# Checar se o container está rodando
for c in "${CONTAINERS[@]}"; do
  ISRUNNING=$(docker ps | grep -o "$c")
  if [ -n "$ISRUNNING" ]; then
    echo "Container $c rodando em `date +%Y-%m-%d:%H:%M:%S`" >> $c.txt
  else
    echo "Container $c reiniciado em `date +%Y-%m-%d:%H:%M:%S`" >> $c.txt
    healthcheck
  fi
done
