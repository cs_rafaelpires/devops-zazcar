#!/bin/bash
#####################################################################
# Program: 
# Description:
#
# Author: NAME <MAIL>
#####################################################################

## Define the working directory and a timestamp
WORK_DIR="$(dirname $0)"
TIMESTAMP="$(date +'%Y%m%d_%H-%M-%S')"

source ${HOME}/.bashrc

rm -rf devops-zazcar || true
git clone https://cs_rafaelpires@bitbucket.org/cs_rafaelpires/devops-zazcar.git
source $WORKSPACE/devops-zazcar/bdd.sh

cd $WORKSPACE/
echo "android-22" > avds.txt
create_avd_from_file avds.txt
wait_for_avd_boot

# Resolve as dependencias do projeto de QA
bundle install || true

CUCUMBER_LIST=""
for emulator in `emulator_list`; do
ADB_DEVICE_ARG="${emulator}" \
bundle exec calabash-android run \
$WORKSPACE/app/build/outputs/apk/app-debug.apk \
-f html -o funcional.html \
-f json -o funcional.json \
-f junit -o test-reports \
-p android features/lista_veiculos.feature -v || true &
CUCUMBER_LIST="${CUCUMBER_LIST} ${!}"
done

wait ${CUCUMBER_LIST}

finish_avds
