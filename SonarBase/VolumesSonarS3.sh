#!/usr/bin/env bash

# Export variables
export PATH="$PATH"
export SONARHOME="/opt/sonarqube"
export DATA="data"
export EXTENSIONS="extensions"
export CIBUCKET="zazcar-ci"
export WORKDIR="/root"

# Download files only first execution
cd $WORKDIR
touch lockS3.lockrun
if [ -f "lockS3.lockrun" ]; then
	echo "Download já realizado" > lockS3.lockrun
else
	/usr/local/bin/aws s3 cp s3://$CIBUCKET/SonarData.tar.gz $WORKDIR
	/usr/local/bin/aws s3 cp s3://$CIBUCKET/SonarExtensions.tar.gz $WORKDIR
	tar -xvzf SonarData.tar.gz -C $SONARHOME
	tar -xvzf SonarExtensions.tar.gz -C $SONARHOME
fi

# Compress files
cd $SONARHOME
tar -cvzf SonarData.tar.gz $DATA
tar -cvzf SonarExtensions.tar.gz $EXTENSIONS

# Copy files to S3
/usr/local/bin/aws s3 cp SonarData.tar.gz s3://$CIBUCKET
/usr/local/bin/aws s3 cp SonarExtensions.tar.gz s3://$CIBUCKET
