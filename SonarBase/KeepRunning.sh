#!/usr/bin/env bash

# Export variables
export PATH="$PATH"

# Start cron
cron && tail -f /var/log/cron.log
