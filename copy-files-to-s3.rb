#!/usr/bin/ruby

require 'aws-sdk-core'

def logs()
s3 = Aws::S3::Client.new
file_list = Dir.glob("/var/jenkins_home/*.xml")
file_list.each {|yo|
  file_open = File.read(yo)
  file_name = File.basename(yo)
  s3.put_object(body: file_open, bucket: "zazcar-ci", key: file_name)
  }
end
